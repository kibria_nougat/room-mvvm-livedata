package com.example.gkibr.roommvvm.ripository

import android.app.Application
import android.os.AsyncTask
import androidx.lifecycle.LiveData
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.gkibr.roommvvm.model.Note
import com.example.gkibr.roommvvm.model.NoteDao
import com.example.gkibr.roommvvm.model.NoteDatabase

class Ripository(application: Application) {


    private var noteDao: NoteDao


    private var allNotes: LiveData<List<Note>>? = null


    init {
        val database: NoteDatabase = NoteDatabase.getInstance(application.applicationContext)!!
        noteDao = database.noteDao()
    }

    fun insertNote(note: Note) {
        InsertNoteAsyncTask(noteDao).execute(note)
    }


    fun deleteAllNotes() {
        DeleteAllNoteAsyncTask(noteDao).execute()
    }


    fun getAllNotes(): LiveData<List<Note>> {
        return noteDao.getAllNotes()
    }


    class PopulateDbAsyncTask(db: NoteDatabase?) : AsyncTask<Unit, Unit, Unit>() {
        private val noteDao = db?.noteDao()

        override fun doInBackground(vararg p0: Unit?) {
            noteDao?.insert(Note("Title 1", "description 1"))
            noteDao?.insert(Note("Title 2", "description 2"))
            noteDao?.insert(Note("Title 3", "description 3"))
        }
    }


    private class InsertNoteAsyncTask(val noteDao: NoteDao) : AsyncTask<Note, Unit, Unit>() {
        override fun doInBackground(vararg p0: Note?) {
            noteDao.insert(p0[0]!!)
        }
    }


    private class DeleteAllNoteAsyncTask(val noteDao: NoteDao) : AsyncTask<Unit, Unit, Unit>() {
        override fun doInBackground(vararg p0: Unit?) {
            noteDao.deleteAllNotes()
        }
    }

}