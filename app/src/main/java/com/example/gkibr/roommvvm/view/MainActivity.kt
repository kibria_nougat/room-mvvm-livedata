package com.example.gkibr.roommvvm.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.gkibr.roommvvm.R
import com.example.gkibr.roommvvm.model.NoteDatabase
import com.example.gkibr.roommvvm.ripository.Ripository

class MainActivity : AppCompatActivity() {


    private val adapter = NoteAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        populateData()


    }


    private fun setAdapter(){

    }

    private fun populateData() {
        object : RoomDatabase.Callback() {
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
                Log.d("DataBaseTesting", "populateData");
                Ripository.PopulateDbAsyncTask(NoteDatabase.getInstance(applicationContext))
                    .execute()
            }
        }
    }
}
